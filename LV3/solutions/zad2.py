# -*- coding: utf-8 -*-
"""
Created on Mon Nov 29 13:38:57 2021

@author: marij
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')
print(len(mtcars))
print(mtcars)

mtcars1 = mtcars.groupby('cyl').mean()
print(mtcars1)
plt.bar([4, 6, 8], mtcars1['mpg'], align='center', alpha= 0.5)
plt.show()

mtcars2 = mtcars[['cyl', 'wt']]
mtcars2.boxplot(by='cyl')
plt.show()



mtcars3 = mtcars.groupby('am').mean()
print(mtcars3)
plt.bar([0, 1], mtcars3['mpg'], align='center', alpha=1, linewidth = 0.1)
plt.show()


plt.scatter(mtcars[mtcars.am == 0]['qsec'], mtcars[mtcars.am == 0]['hp'], label='automatic', s=mtcars[mtcars.am == 0]['wt']*20)
plt.scatter(mtcars[mtcars.am == 1]['qsec'], mtcars[mtcars.am == 1]['hp'], label='manual', s=mtcars[mtcars.am == 1]['wt']*20)
plt.legend(loc=1)
plt.show()

print(mtcars.tail(4))