# -*- coding: utf-8 -*-
"""
Created on Fri Nov 12 19:03:37 2021

@author: marij
"""

import numpy as np
import matplotlib.pyplot as plt

np.random.seed(0)
die = np.random.randint(low=1, high=7, size=100)
print(die)
plt.hist(die, bins=20)